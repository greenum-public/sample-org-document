
#+DESCRIPTION: Demo org document
#+LANGUAGE:  en
#+OPTIONS:   H:4
# +OPTIONS:   toc:t
#+OPTIONS:   num:9

# Document Metadata Macros
#+MACRO: Plain_Title Specification for modifying the org-export command-line org document build scripts
#+MACRO: Revision (Rev 1.3)

#+PROPERTY:  header-args :padline no
#+SETUPFILE: org/theme-readtheorg-local.setup
#+SETUPFILE: org/latex_setup.org
#+BIBLIOGRAPHY: bib/orgmode.bib
#+CITE_EXPORT: csl styles/apa.csl

#+AUTHOR: Q Jones
#+TITLE: {{{Plain_Title}}} {{{Revision}}}


# Convenience Macros
#+MACRO: OM Org Mode



#+LATEX: \newpage

* Introduction and Scope

This document captures specification for adding the required packages and
configuration to the org-export scripts (https://github.com/nhoffman/org-export)
for emacs org-mode documents,

* Description

Org mode is a superior markup language and a tool for generating complex
documents. However, org is intimately tied to emacs, which is generally used
interactively.[cite:@OrgMode] Often documents generated using org markup take
advantage of additional Emacs perks such as live embedded code blocks. The extra
abilities are out of scope for alternate org-rendering engines such as pandoc.
So emacs support for org is first-class and non-emacs renderers provide
second-class support.

The export-org project (https://github.com/nhoffman/org-export) addresses this
problem by setting up emacs for batch use to generate documents from org files.

* Deliverables

For this project, the export-org project will be extended to handle the provided
Latex setup in ~org/latex_setup.org~, the HTML read-the-org theme in ~src~ and
css definitions in the ~org~ directory, with the provided sample script. The
output should be identical to the manually generated spec.pdf and spec.html
files which can be viewed on the [[https://sample-org-document-greenum-public-91ab3e5cce98b1d8dc319a2389e3.gitlab.io/][Gitlab Page]].

* Requirements:

1) The updated scripts shall run on the at least Emacs 29.1 and Org 9.6

2) The scripts shall find the plantuml.jar file. If needed, the location can be
   specified by an environment variable or a config file.

3) The solution shall render this document identically to the provided PDF and
   HTML outputs.

* Overal Build process

The emacs batch org-file publishing will be part of a larger automated process.
The scripting and CI components are beyond the scope of this project.

#+name: Build Process
#+BEGIN_SRC plantuml :results file :file build-process.png :output-dir images
@startuml hide empty description top to bottom direction

skinparam titleBorderRoundCorner 15
skinparam titleBorderThickness 2
skinparam titleBorderColor black
skinparam defaultTextAlignment<<left>> left

title "Build Process"

state environment {
    environment: CI image
    environment: Emacs installation
    environment: Emacs package installation

    state doc_proc as "Document Processing" {
        state ci as "Continuous Integration" {
            ci: Gitlab CI yaml script
            state make as "Makefile"{
                state bash as "Shell Scripts" {
                    bash: Bash script for PDF output
                    bash: Bash script for HTML output
                    bash: Bash script to test for errors
                    state emacs {
                        emacs: bare-bones .init to configure org-mode for output
                    }
                }
            }
        }
    }
}
#+END_SRC

#+RESULTS: Build Process
[[file:images/build-process.png]]

- Batch-Mode Emacs Configuration :: There needs to be a repeatable, modularized
  method for configuring a batch version of emacs with the necessary packages
  required to generate documents. The config files should be clearly written and
  contain sufficient documentation to allow extension if more components are
  needed later, or, for example, a component needs to be swapped out (such as a
  citation system).

  For performance, the configuration may be broken out into to components:

  1) Package installation, which may be done once for all documents (if, for
     example, several documents are built in a CI script).

  2) Package loading and configuration, which would be done once for each
     document produced in a session. This should ensure all required packages
     are available and properly configured to produce the same output from a
     batch run as would be generated interactively.

- Shell Scripts :: Bash-compatible shell scripts to invoke emacs from the command line to
  accomplish the package installation and document creation functions. The emacs
  invocations should not include any hard-coded filenames. Any filenames should
  be passed as parameters. There may be separate scripts to generate PDF and
  HTML outputs, or a single script may be used to generate one or the other
  depending on a command-line switch. There should also be a shell script to
  test the org file for errors without generating output.

- Makefile :: A Makefile to automate generation of a PDF document or an HTML document, or
  both.

- Gitlab CI setup :: A gitlab-ci.yml file shall be created to automate the
  generation of PDF and HTML documents and publish them to pages. This
  deliverable includes:

  1) Selecting an appropriate image. This could be a generic Linux image on
     which emacs is installed along with necessary packages, or an image already
     containing a pre-built Emacs.

  2) Ensuring than any necessary latex packages are installed to fully compile
     the file. A pre-built docker image with Emacs and Latex may be ideal.

- Accessory files :: Any additional files required shall be included in a CI build.  Such as:

  - a plantuml.jar file

  - CSL style files for citation formatting.  I have included them in the ./styles directory.


* References

#+PRINT_BIBLIOGRAPHY:
